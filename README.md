# block-messenger

Simplifies message exchange between pipeline blocks.

Factory Pattern simplifies replacement of Client. 

MQTT Client is Implemented based on NPM [mqtt package](https://www.npmjs.com/package/mqtt)

To Create a MQTT Client using the BlockMessenger Factory

```type = 'mqtt'```

To receive messages, listen to event 'message' (topic, message)
To publish a message, messenger.publish(some_data, callback);

## Usage example: 

```
'use strict';

const Messenger = require('block-messenger');

const options = {
    clientId: 'test',
    host: process.env.MQTT_HOST || '127.0.0.1',
    port: process.env.MQTT_PORT || 1883,
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD,
    qos: 1
};

const mqtt = Messenger.mqtt(options);

console.log(options);

mqtt.on('subscribed', (granted) => {
    console.log(granted);
});

mqtt.on('message', (topic, message) => {
    console.log(`new message in topic ${topic}:`, message.toString());
});


mqtt.subscribe('aigent/test/in');

setInterval(() => {
    mqtt.publish('aigent/test/out', {data: 'test data'})
        .then(data => {
            console.log('data published');
        }).catch(err => {
        console.error(err);
    });

}, 1000);
```

## The MQTT Client also emits the following events:

* connect
* reconnect
* close
* offline
* error
* end
* packetsend
* packetreceive
* message
* subscribed
 
 
## Run Tests

```
npm test
npm run-script test-cov
```

## TO DO: 

- Implement kafka-messenger.js (type = 'kafka')


## Dev:

Transpile TypeScript into Javascript:

```
npm install
npm run-script build
npm publish
```

