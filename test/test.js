'use strict';

const Messenger = require('../dist/index');

const options = {
    clientId: 'test',
    host: process.env.MQTT_HOST || '127.0.0.1',
    port: process.env.MQTT_PORT || 1883,
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD,
    qos: 1
};


const mqtt = new Messenger.mqtt(options);

console.log(options);

mqtt.on('subscribed', (granted) => {
    console.log(granted);
});

mqtt.on('message', (topic, message) => {
    console.log(`new message in topic ${topic}:`, message.toString());
});

mqtt.subscribe('aigent/test/in');

setInterval(() => {
    mqtt.publish('aigent/test/out', {data: 'test data'})
        .then(data => {
            console.log('data published');
        }).catch(err => {
        console.error(err);
    });

}, 1000);
