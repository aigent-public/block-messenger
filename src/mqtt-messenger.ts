import {IMessenger, IMqttMessengerOptions, IMqttOptions} from "./messenger-interface";
import {EventEmitter} from 'events';
import * as Mqtt from 'mqtt';

const uuid = require('uuid/v4');

/**
 *
 * =================
 * Events:
 * =================
 *  - connect
 *  - reconnect
 *  - close
 *  - offline
 *  - error
 *  - end
 *  - packetsend
 *  - packetreceive
 *  - message
 *  - subscribe
 *  - publish
 *
 */

export class MqttMessenger extends EventEmitter implements IMessenger {

    qos: 0 | 1 | 2;
    options: IMqttOptions;
    mqtt: Mqtt.MqttClient;

    constructor(options: IMqttMessengerOptions) {
        super();

        this.qos = options.qos || 0;
        this.options = {
            clientId: `${options.clientId }-${uuid()}`,
            resubscribe: true,
            reconnectPeriod: 1000,
            host: options.host || '127.0.0.1',
            port: options.port || 1883
        };

        if (options.username) {
            this.options.username = options.username;
        }
        if (options.password) {
            this.options.password = options.password;
        }

        this.mqtt = Mqtt.connect(this.options);
        this.addEvents();

    }

    private addEvents() {
        // this.mqtt.on('connect', () => {
        //     this.emit('connect');
        // });
        //
        // this.mqtt.on('reconnect', () => {
        //     this.emit('reconnect');
        // });
        //
        // this.mqtt.on('close', () => {
        //     this.emit('close');
        // });
        //
        // this.mqtt.on('offline', () => {
        //     this.emit('offline');
        // });
        //
        // this.mqtt.on('error', (error) => {
        //     this.emit('error', error);
        // });
        //
        // this.mqtt.on('end', () => {
        //     this.emit('end');
        // });
        //
        // this.mqtt.on('packetsend', (packet) => {
        //     if (packet.cmd === 'publish') {
        //         this.emit('packetsend', packet);
        //     }
        // });
        //
        // this.mqtt.on('packetreceive', (packet) => {
        //     if (packet.cmd === 'publish') {
        //         this.emit('packetreceive', packet);
        //     }
        // });

        this.mqtt.on('message', (topic, message) => {
            this.emit("message", topic, message);
        });
    }

    subscribe(topic: string) {
        let self = this;
        return new Promise((resolve, reject) => {
            self.mqtt.subscribe(topic, {qos: self.qos}, (err, granted) => {
                if (err) {
                    reject(err);
                }
                else {
                    this.emit("subscribe", granted);
                    resolve(granted);
                }
            });
        });
    }

    publish(topic: string, data: object) {
        let self = this;
        return new Promise((resolve, reject) => {
            // Convert Objects and arrays to String
            let message = JSON.stringify(data);
            self.mqtt.publish(topic, message, {qos: self.qos}, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    this.emit("publish", topic, message);
                    resolve(message);
                }
            });
        });
    };

}
