export interface IMessenger {
    publish: (topic: string, data: object) => Promise<any>;
    subscribe: (topic: string) => Promise<any>;
    on: (event: string, callback: any) => void;
}

export type IMqttMessengerOptions = {
    clientId: string;
    host: string;
    port: number;
    qos?: 0 | 1 | 2;
    username?: string;
    password?: string;
}

export type IMqttOptions = {
    clientId: string;
    resubscribe: boolean;
    reconnectPeriod: number;
    host: string;
    port: number;
    username?: string;
    password?: string;
}